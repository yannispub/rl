import cv2
from enduro.agent import Agent
from enduro.action import Action
from enduro.state import EnvironmentState
import numpy
import sys
import matplotlib.pyplot as plt
from matplotlib import cm

class RandomAgent(Agent):
    def __init__(self):
        super(RandomAgent, self).__init__()
        # Add member variables to your class here
        self.total_reward = 0
        self.rewardPerEp = []
        
    def initialise(self, grid):
        """ Called at the beginning of an episode. Use it to construct
        the initial state.
        """
        cv2.imshow("Enduro", self._image)
        cv2.imshow("Environment Grid", EnvironmentState.draw(grid))
        
        # Reset the total reward for the episode
        self.total_reward = 0

               
    def act(self):
        """ Implements the decision making process for selecting
        an action. Remember to store the obtained reward.
        """

        # Get random action
        rndIdx = numpy.random.random_integers(0,3);
        action =  self.getActionsSet()[rndIdx];
        #print Action.toString(action);

        # Execute the action and get the received reward signal
        self.total_reward += self.move(action)

    def sense(self, grid):
        """ Constructs the next state from sensory signals.

        gird -- 2-dimensional numpy array containing the latest grid
                representation of the environment
        """
        # Visualise the environment grid
        cv2.imshow("Environment Grid", EnvironmentState.draw(grid))

    def learn(self):
        """ Performs the learning procedure. It is called after act() and
        sense() so you have access to the latest tuple (s, s', a, r).
        """
        pass

    def callback(self, learn, episode, iteration):
        """ Called at the end of each timestep for reporting/debugging purposes.
        """
        print "{0}/{1}: {2}".format(episode, iteration, self.total_reward)
        if iteration>=6500:
           self.rewardPerEp.append(self.total_reward)
        # Show the game frame only if not learning
        if not learn:
            cv2.imshow("Enduro", self._image)
            cv2.waitKey(40)

if __name__ == "__main__":
    a = RandomAgent()
    # The agent always follows a random policy, with no learning involved.
    episodes = 100
    a.run(True, episodes, draw=True)
    plt.plot(a.rewardPerEp)
    plt.title("Total reward per episode.")
    plt.xlabel('Episode')
    plt.ylabel('Total Reward')
    axes = plt.gca()
    axes.set_xlim([1,episodes])
    plt.savefig('/home/yannis/Desktop/RandomAgent_RewardPerEpisode.png')
    plt.show()
    plt.close()
    print "The mean of the total rewards is ",numpy.mean(a.rewardPerEp)," and the variance is ",numpy.var(a.rewardPerEp),"."
    plt.hist(a.rewardPerEp, bins=[-1,0,1,2,3,4,5,6,7,8,9,10],normed=True)
    axes = plt.gca()
    axes.set_xlim([-1,10])
    axes.set_ylim([0,1])
    plt.title("Distribution over reward per episode.")
    plt.savefig('/home/yannis/Desktop/RandomAgent_DistributionRewardPerEpisode.png')
    plt.show()
    plt.close()
    print 'Total reward: ' + str(a.total_reward)
