import cv2
from enduro.agent import Agent
from enduro.action import Action
from enduro.state import EnvironmentState
import numpy
import sys
import matplotlib.pyplot as plt
from matplotlib import cm
import random
import cPickle

class QAgent(Agent):
    def __init__(self,eps,gamma,T,train_episodes):
        super(QAgent, self).__init__()
   
        # Eps controls the degree of exploration
        self.eps = eps
        # Aplha is the learning rate
        self.alpha = 0
        # Gamma is the discount factor
        self.gamma = gamma
        self.episodes = 0
        self.frame = 1.0
        self.total_reward = 0
        self.current_reward = 0
        # Store previous world grid.
        self.prev_grid = None
        # Dictionary containing action-value function, will be initialized as each state is met. State is row 0 of the gridworld.
        self.Q = dict()
        # Previous state
        self.prev_state = None
        # Current state
        self.curr_state = None
        # Initial state
        self.init_state = None
        # Last action taken
        self.last_action = None
        self.rewardPerEp = []
        # Look-ahead horizon
        self.T = T
        # Flags to control learning-evaluation
        self.train_episodes = train_episodes
        self.rnd = 1
        self.evl = 0
        self.stopLearn = 0
        self.eval_ep = 0

    def initialise(self, grid):
        """ Called at the beginning of an episode. Use it to construct
        the initial state.
        """

        #Initialize variable storing previous world and get init state
        self.prev_grid = grid
        state = []
        for i in xrange(10):
            state.append(grid[0:self.T-1,i])
        self.init_state = str(state)
        
        ##cv2.imshow("Enduro", self._image)
        ##cv2.imshow("Environment Grid", EnvironmentState.draw(grid))
            
        # Reset the total reward for the episode
        self.total_reward = 0

        

        
    def act(self):
        """ Implements the decision making process for selecting
        an action. Remember to store the obtained reward.
        """

        if ((self.evl) or (self.stopLearn==0)):
           q = []
           #Get Q(s,a) value if (s,a) has been seen before, else action-value will be zero
           for action in [0,1,2,3]:
             if (self.curr_state,action) in self.Q:
                 q.append(self.Q.get((self.curr_state,action)))
             else:
                 q.append(0)
           maxQ = max(q)

           #Even when we explore, take Q(s,a) into account
           if random.random() < self.eps:
                minQ = min(q)
                scale = max(abs(minQ), abs(maxQ))
                # Add random values to all the actions, recalculate maxQ
                q = [ q[i] + random.random() * scale - 0.5 * scale for i in [0,1,2,3] ] 
                maxQ = max(q)

           if q.count(maxQ) > 1:
               best = [i for i in [0,1,2,3] if q[i] == maxQ]
               i = random.choice(best)
           else:
               i = q.index(maxQ)

        else:
             i = numpy.random.random_integers(0,3);

        action = self.getActionsSet()[i]
        self.last_action = i
        self.current_reward = self.move(action)

        self.total_reward += self.current_reward

        
    def sense(self, grid):
        """ Constructs the next state from sensory signals.

        grid -- 2-dimensional numpy array containing the latest grid
                representation of the environment
        """

        #Get prev state
        state = []
        for i in xrange(10):
            state.append(self.prev_grid[0:self.T-1,i])
        self.prev_state = str(state)

        #Get current state
        state = []
        for i in xrange(10):
            state.append(grid[0:self.T-1,i])
        self.curr_state = str(state)

        #Update self.prev_grid
        self.prev_grid = grid
       
        # Visualise the environment grid
        cv2.imshow("Environment Grid", EnvironmentState.draw(grid))


    def learn(self):
        """ Performs the learning procedure. It is called after act() and
        sense() so you have access to the latest tuple (s, s', a, r).
        """
        
        if self.stopLearn == 0:
                self.alpha = 1.0/(self.frame**0.4) #**0.4
                # State-action already seen
                if (self.prev_state, self.last_action) in self.Q:
                    maxqnew = max([self.Q.get((self.curr_state, a)) for a in [0,1,2,3]])
                    if maxqnew==None:
                        maxqnew = 0.0
                    oldv = self.Q.get((self.prev_state, self.last_action))
                    self.Q[(self.prev_state, self.last_action)] = oldv + self.alpha * (self.current_reward + self.gamma*maxqnew - oldv)    
                else:
                    #Add new state-action pair
                    self.Q[(self.prev_state, self.last_action)] = self.current_reward
        else:
            #print "no learn"
            pass                    
            
            
    def callback(self, learn, episode, iteration):
        """ Called at the end of each timestep for reporting/debugging purposes.
        """
        self.frame = self.frame+1
        print "{0}/{1}: {2}".format(episode, iteration, self.total_reward)
        if episode>self.train_episodes:
            self.stopLearn = 1
            if iteration>=6500:
                if self.evl:
                    self.rewardPerEp.append(self.total_reward)
                    self.evl = 0
                    self.rnd = 1
                    self.eval_ep = self.eval_ep + 1
                elif self.rnd:
                    self.evl = 1
                    self.rnd = 0
        
        # Show the game frame only if not learning
        if not learn:
            cv2.imshow("Enduro", self._image)
            cv2.waitKey(40)

            
if __name__ == "__main__":
    train_episodes = 1000
    eval_episodes = 100
    """
    a = QAgent(eps=0.001,gamma=0.9,T=3,train_episodes=train_episodes) #try with e=0.01
    a.episodes = train_episodes + 2*eval_episodes 
    a.run(True, a.episodes, draw=False)
    plt.plot(a.rewardPerEp)
    plt.xlabel('Episode')
    plt.ylabel('Total Reward')
    axes = plt.gca()
    axes.set_xlim([1,a.eval_ep])
    plt.savefig('/home/yannis/Desktop/QAgent_RewardPerEpisode100.png')
#    plt.show()
    plt.close()
    print "The mean of the total rewards is ",numpy.mean(a.rewardPerEp)," and the variance is ",numpy.var(a.rewardPerEp),"."
    plt.hist(a.rewardPerEp, bins='auto',normed=True)
    axes = plt.gca()
    axes.set_ylim([0,1])
    plt.title("Distribution over reward per episode.")
    plt.savefig('/home/yannis/Desktop/QAgent_DistributionRewardPerEpisode100.png')
#    plt.show()
    plt.close()
    print 'Total reward: ' + str(a.total_reward)
    
    """
    #Compare learning curves with varying horizon
    b = QAgent(eps=0.001,gamma=0.9,T=6,train_episodes=train_episodes)
    b.episodes = train_episodes + 2*eval_episodes 
    b.run(True, b.episodes, draw=False)
    with open("/home/yannis/Desktop/b.pickle", "wb") as output_file:
        cPickle.dump(b.rewardPerEp, output_file)

    """
    c = QAgent(eps=0.001,gamma=0.9,T=9,train_episodes=0)
    c.run(True, 200, draw=False)
    d = QAgent(eps=0.001,gamma=0.9,T=11,train_episodes=0)
    d.run(True, 200, draw=False)

    plt.plot(a.rewardPerEp, 'r--', b.rewardPerEp, 'bs', c.rewardPerEp,  'g^', d.rewardPerEp, 'y+')
    plt.xlabel('Episode')
    plt.ylabel('Total Reward')
    axes = plt.gca()
    axes.set_xlim([1,a.episodes])
    plt.savefig('/home/yannis/Desktop/TimeHorizonLCurves.png')
#    plt.show()
    plt.close()
    """

    
